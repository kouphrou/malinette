# La Malinette
A Free Open Source Kit To Simplify Programming Interactivity

- Version: 1.0.beta
- Languages: English, French, Spanish (~)
- Date: 21/10/2016
- Contact: contact_at_reso-nance.org
- Website: http://reso-nance.org/malinette
- Licence: GNU/GPL v3

Special thanks for all Pure Data developers, we've taken some good ideas from few great projects (pdmtl abstractions, DIY2, ...)

## Description
Malinette is an all-in-one solution, a rapid prototyping tool to show and make simple interaction and multimedia projects. It is a software and a hardware set. The software is based on Pure Data extended. It is also a very convenient suitcase and wood boxes with sensors, actuators, Arduino, Teensy and electronic components to test ideas and projects very quickly.

## Features
- In english : http://reso-nance.org/malinette/en/software/features
- In french : http://reso-nance.org/malinette/fr/software/features

## Requirements
- Computer : a decent computer (>2005 is adviced)
- Operating system : should work on Mac OS X, MS Windows and main GNU/Linux distributions.
- Install Pd-extended : http://puredata.info/downloads/pd-extended
- Optional, if you want to use an Arduino, download the Arduino software : http://www.arduino.cc/en/Main/Software. Plug the USB board, install drivers if required, and upload the "StandardFirmata" sketch from the Arduino software : Menu File > Examples > Firmata > StandardFirmata

## Pure Data dependencies
If you have not Pure Data Extended, here you can find our dependencies :  creb, initbang, Gem, zexy, cyclone, moonlib, moocow, hcs, tof, comport, iemlib, iemnet or oscx or mrpeach, markex, motex, ggee, list-abs, mapping, pix_fiducialtrack, sigpack, pmpd, purepd

You should download them with the new pd.0.47 feature "Find externals" (Deken).

## Installation and startup
1. Download the software:
    - The most recent version : https://git.framasoft.org/resonance/malinette
    - Release : http://reso-nance.org/malinette/
2. Extract anywhere
3. Open the "MALINETTE.pd"
4. Open Manual examples to see how it works

## User instructions
You have two windows : the menu on the left and the project window on the right (called a "patch" in Pure Data). Basically, you can open examples or create your project. When you want to start a project, the better way is to open the "new" project and fill it with some objects. Clic on the "?" buttons to find all objects of a category. You can also find some documentations in the "./docs" folder.

Have fun and report feedbacks and bugs at contact_/at/_reso-nance.org.

## Content
- ./abstractions    : all objects (audio, core, in, numbers, out, seq, video)
- ./medias          : media files (images, videos, sounds, etc.)
- ./docs            : some documentations (license, todo, changes) and OVERVIEW.pdf help
- ./examples        : examples patchs to show and edit projects about interaction
- ./other           : tclplugins, scripts, processing codes, ...
- ./projects        : your projects folders
- MALINETTE.pd      : open this patch to start the program (in french)
- preferences.txt   : preferences file with global setup (language, level, screen size, project folder)
- README.md		

## Abstractions
- ./abstractions/audio      : audio effects
- ./abstractions/core       : core functions (menu buttons, media masters, tools, ...)
- ./abstractions/in         : inputs (arduino, audio, video, mouse, key, kinect, ...)
- ./abstractions/numbers    : numbers effects
- ./abstractions/out        : outputs (arduino, audio, video, ...)
- ./abstractions/seq        : sequencers tools
- ./abstractions/video      : video effects

## Coding rules
- file names with dash
- file names in lowercase
- default values
- test arguments
- a toggle to switch on/off the abstraction
- when an extra object is used from pd-extended, put the library as a prefix. Ex: [tof/menubutton] (to discuss)
- if they are more than 2 or 3 inlets, a good practice would be to use the cold inlet (on the right) to pass messages throught it and route them with a [route] object.

## Files formats (video, audio, 3d etc..)
- Video : best format for video files (better performance in pd) is MJPEG, this compression is better packed in .avi for windows and .mov for mac.
- Audio : Pure Data audio objects use .aiff or .wav, except some librairies which use mp3 (we do not have mp3 player yet).
- 3d: .obj is the 3d-model-file you can import in Pure Data, you can create .obj from most of 3D software (like Blender for instance).

## Some documentations (in french)
- Pure Data : http://fr.flossmanuals.net/Puredata/
- Arduino : http://fr.flossmanuals.net/arduino/
- Forum de bidouilleurs : http://codelab.fr/
