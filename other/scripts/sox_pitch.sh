#! /bin/sh
# Use Sox software to create a lot of audio files pitched

file="$1"
folder="$(basename "$file" | cut -d. -f1)"
ext="$(basename "$file" | cut -d. -f2)"

# Create folder
if [ -d $folder ]
then
	rm -rf $folder
	mkdir $folder
else
	mkdir $folder
fi

# Use Sox loop
#for i in {0..127}
for i in `seq 0 127`
do 
	#echo "$folder-$i.$ext"
	sox "$file" "$folder/$folder-$i.$ext" pitch $(( ($i * 100) - ((127/2)*100) ))
done

exit 0
