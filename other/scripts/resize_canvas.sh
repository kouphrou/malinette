#! /bin/sh
# Reset all sizes and position of the main patch and examples patches
# to display it at the same size and position


# Change the first line of the MALINETTE.pd patch
cp ../../MALINETTE.pd f.back
sed '1 s/^.*$/#N canvas 0 0 150 687 10;/g' f.back > ../../MALINETTE.pd
rm f.back

# Change the first line of the examples/basics patchs
for file in ../../examples/examples-en/*.pd
do
    cp $file f.back
    sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back

# Change the first line of the examples/basics patchs
for file in ../../examples/examples-fr/*.pd
do
    cp $file f.back
    sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back


# Change the first line of the examples/basics patchs
for file in ../../examples/manual-en/*.pd
do
    cp $file f.back
    sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back

# Change the first line of the examples/basics patchs
for file in ../../examples/manual-fr/*.pd
do
    cp $file f.back
    sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back



# Change the first line of the examples/basics patchs
for file in ../../examples/manual-es/*.pd
do
    cp $file f.back
    #~ sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back


# Change the first line of the examples/basics patchs
for file in ../../examples/examples-es/*.pd
do
    cp $file f.back
    sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back


# Change the first line of the examples/basics patchs
for file in ../../examples/examples-en/*.pd
do
    cp $file f.back
    sed '1 s/^.*$/#N canvas 151 22 800 687 10;/g' f.back > $file
done
rm f.back
