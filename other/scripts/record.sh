#!/bin/bash
# Script to screencast audio and video (ffmpeg for video, vlc for audio, jack and pulse audio to connect Pure Data to vlc)
DATE=`date +%Y%m%d`
TIME=`date +%Hh%M`

# Change the destination files
AUDIO_FILE="/home/jr/Bureau/record/out-$DATE-$TIME.wav"
VIDEO_FILE="/home/jr/Bureau/record/out-$DATE-$TIME.avi"

# Start screencast
cp ~/.pdextended_malinette ~/.pdextended
qjackctl -a ~/Softs/jack-patchbay.xml &
sleep 3
pd-extended -jack -open /media/data/code/malinette/malinette/MALINETTE-en.pd &
sleep 3
ffmpeg -f x11grab -r 25 -s 946x768 -i :0.0+0,0 -vcodec huffyuv -y $VIDEO_FILE &
cvlc jack://channels=2:ports=.* --jack-input-auto-connect --sout="#transcode{acodec=s16l,channels=2,ab=128,samplerate=44100}:standard{access=file,mux=wav,dst="$AUDIO_FILE"}"

# Kill
killall qjackctl
killall pd-extended
killall ffmpeg
killall cvlc

#jack_connect pd_extended_0:output0 "PulseAudio JACK Source:front-left"
#jack_connect pd_extended_0:output1 "PulseAudio JACK Source:front-right"
#--sout "#transcode{acodec=mp3,ab=128,channels=2,samplerate=44100}:duplicate{dst=std{access=file,mux=mp4,dst=$AUDIO_FILE}}}"
